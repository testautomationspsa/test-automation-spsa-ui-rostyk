package com.epam.bo;

import com.epam.po.pages.HomePage;
import com.epam.utils.SoftAssert;

public class HomeBO {
    private HomePage homePage = new HomePage();

    public void navigateToHomePage(){
        homePage.navigateToSportPartner();
    }

    public void verifyIfAllNecessaryButtonsArePresent(){
        SoftAssert softAssert = new SoftAssert();
        softAssert.assertTrue(homePage.isHomeButtonDisplayedAndEnabled(), "Home Button is not present");
        softAssert.assertTrue(homePage.isBlogButtonDisplayedAndEnabled(), "Blog Button is not present");
        softAssert.assertTrue(homePage.isLoginButtonDisplayedAndEnabled(), "Login Button is not present");
        softAssert.assertTrue(homePage.isSignUpButtonDisplayedAndEnabled(), "SignUp Button is not present");
        softAssert.assertAll();
    }

    public void goToLoginPage(){
        homePage.clickLoginButton();
    }

    public void goToBlogPage(){
        homePage.clickBlogButton();
    }

    public void goToHomePage(){
        homePage.clickHomeButton();
    }

    public void goToRegisterPage(){
        homePage.clickSignUpButton();
    }
}
