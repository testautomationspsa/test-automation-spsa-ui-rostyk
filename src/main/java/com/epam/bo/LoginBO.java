package com.epam.bo;

import com.epam.po.pages.LoginPage;
import com.epam.utils.SoftAssert;

import static org.testng.Assert.assertEquals;

public class LoginBO {
    private LoginPage loginPage = new LoginPage();

    public void fillEmailAndPassword(String login, String password){
        loginPage.fillEmailField(login);
        loginPage.fillPasswordField(password);
    }

    public void authorize(String login, String password) {
        fillEmailAndPassword(login, password);
        loginPage.clickSubmitButton();
    }

    public void verifyIfAllNecessaryFieldArePresent() {
        SoftAssert softAssert = new SoftAssert();
        softAssert.assertTrue(loginPage.isEmailFieldDisplayedAndEnabled(), "Email Field is not present");
        softAssert.assertTrue(loginPage.isPasswordFieldDisplayedAndEnabled(), "Password Field is not present");
        softAssert.assertTrue(loginPage.isSubmitButtonDisplayedAndEnabled(), "Submit Button is not present");
        softAssert.assertAll();
    }

    public void verifySubmitButton(boolean isActive) {
        assertEquals(isActive, loginPage.isSubmitButtonDisplayedAndEnabled(),
                "Submit button is " + (isActive ? "" : "not") + " present");
    }

}
