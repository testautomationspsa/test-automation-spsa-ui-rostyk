package com.epam.bo;

import com.epam.po.pages.RegistrationPage;
import com.epam.utils.SoftAssert;
import org.testng.annotations.Test;

import java.io.IOException;

import static com.epam.utils.Constants.NEW_USER_EMAIL;
import static com.epam.utils.DataFactory.getUsersLastMessage;
import static io.restassured.RestAssured.given;
import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertNotNull;

public class RegisterBO {
    private RegistrationPage registrationPage = new RegistrationPage();
    private String activationLink;

    public void fillEmailAndPassword(String login, String password, String confirmPassword) {
        registrationPage.fillEmailField(login);
        registrationPage.fillPasswordField(password);
        registrationPage.fillConfirmPasswordField(confirmPassword);
    }

    public void register(String login, String password, String confirmPassword) {
        fillEmailAndPassword(login, password, confirmPassword);
        registrationPage.clickSubmitButton();
    }

    public void verifyIfAllNecessaryFieldArePresent() {
        SoftAssert softAssert = new SoftAssert();
        softAssert.assertTrue(registrationPage.isEmailFieldDisplayedAndEnabled(), "Email Field is not present");
        softAssert.assertTrue(registrationPage.isPasswordFieldDisplayedAndEnabled(), "Password Field is not present");
        softAssert.assertTrue(registrationPage.isConfirmPasswordFieldDisplayedAndEnabled(), "Confirm Password Field is not present");
        softAssert.assertTrue(registrationPage.isSubmitButtonDisplayedAndEnabled(), "Submit Button is not present");
        softAssert.assertAll();
    }

    public void verifyIfActivationLinkIsSentToEmail() throws IOException, InterruptedException {
        activationLink = getUsersLastMessage(NEW_USER_EMAIL);
        System.out.println(activationLink);
        assertNotNull(activationLink, "Activation link is empty");
    }

    @Test
    public void  g() throws IOException, InterruptedException {
        assertEquals(getUsersLastMessage(NEW_USER_EMAIL), "https://epam-spsa-app.herokuapp.com/activate-user?token=eyJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJuYXphci5mZWRjaHVrMTNAZ21haWwuY29tIiwiZXhwIjoxNTg4MDYwMDU1LCJpYXQiOjE1ODgwNTY0NTV9.3rc8bPLv5mu2tascnuhzDbTAHeUFRf4QXNb53Plvbfw");
    }

    public void activateUser() {
        given().log().all().get(activationLink).then().log().all();
    }

    public void verifySubmitButton(boolean isActive) {
        assertEquals(isActive, registrationPage.isSubmitButtonDisplayedAndEnabled(),
                "Submit button is " + (isActive ? "" : "not") + " present");
    }

}
