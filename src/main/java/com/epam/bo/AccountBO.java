package com.epam.bo;

import com.epam.po.pages.AccountPage;
import com.epam.utils.SoftAssert;

import static org.testng.Assert.assertTrue;

public class AccountBO {
    private AccountPage accountPage = new AccountPage();

    public void verifyIfAllNecessaryButtonsArePresent(){
        SoftAssert softAssert = new SoftAssert();
        softAssert.assertTrue(accountPage.isSearchButtonDisplayedAndEnabled(), "Search button is not present");
        softAssert.assertTrue(accountPage.isProfileButtonDisplayedAndEnabled(), "Profile button is not present");
        softAssert.assertTrue(accountPage.isAchievementsButtonDisplayedAndEnabled(), "Achievements Button is not present");
        softAssert.assertTrue(accountPage.isRequestsButtonDisplayedAndEnabled(), "Requests Button is not present");
        softAssert.assertTrue(accountPage.isAccountButtonDisplayedAndEnabled(), "Account Button is not present");
        softAssert.assertAll();
    }

    public void verifyIfUserIsOnAccountPage(){
        assertTrue(accountPage.isUserOnAccountPage(), "User is not on the account page ...");
    }

    public void logOut(){
        accountPage.clickMenuButton();
        accountPage.clickLogOutButton();
    }

    public void deleteAccount() {
        accountPage.clickDeleteAccountButton();
    }
}
