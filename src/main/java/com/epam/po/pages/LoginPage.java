package com.epam.po.pages;

import com.epam.elements.implementations.Button;
import com.epam.elements.implementations.TextInput;
import com.epam.po.BasePage;
import io.qameta.allure.Step;
import org.openqa.selenium.support.FindBy;

public class LoginPage extends BasePage {
    @FindBy(xpath = "//input[@formcontrolname='email']")
    private TextInput emailInput;
    @FindBy(xpath = "//input[@formcontrolname='password']")
    private TextInput passwordInput;
    @FindBy(xpath = "//button/span[text()='SUBMIT']/..")
    private Button submitButton;
    @FindBy(xpath = "//button/span[contains(text(),'FACEBOOK')]/..")
    private Button facebookSignInButton;
    @FindBy(css = "a[href='/sign-up']")
    private Button signUpButton;

    @Step("Filling emailField with email = {0} ...")
    public void fillEmailField(String email){
        emailInput.sendKeys(email);
    }

    @Step("Filling passwordField with password = {0} ...")
    public void fillPasswordField(String password){
        passwordInput.sendKeys(password);
    }

    @Step("Clicking submit button ...")
    public void clickSubmitButton(){
        submitButton.click();
    }

    @Step("Clicking facebookSignIn button ...")
    public void clickFacebookSignInButton(){
        facebookSignInButton.click();
    }

    @Step("Clicking SignUp button ...")
    public void clickSignUpButton(){
        signUpButton.click();
    }

    @Step("Verifying if login field is displayed and enabled ...")
    public boolean isEmailFieldDisplayedAndEnabled(){
        return emailInput.isDisplayed() && emailInput.isEnabled();
    }

    @Step("Verifying if password field is displayed and enabled ...")
    public boolean isPasswordFieldDisplayedAndEnabled(){
        return passwordInput.isDisplayed() && passwordInput.isEnabled();
    }

    @Step("Verifying if submit button is displayed and enabled ...")
    public boolean isSubmitButtonDisplayedAndEnabled(){
        return submitButton.isEnabled() && submitButton.isDisplayed();
    }

}
