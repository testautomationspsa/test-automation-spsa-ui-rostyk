package com.epam.po.pages;

import com.epam.elements.BaseElement;
import com.epam.elements.implementations.Button;
import com.epam.po.BasePage;
import com.epam.utils.WebWait;
import io.qameta.allure.Step;
import org.openqa.selenium.support.FindBy;

import static com.epam.utils.Constants.ACCOUNT_PAGE_URL;

public class AccountPage extends BasePage {
    @FindBy(id = "mat-tab-label-0-0")
    private Button profileButton;
    @FindBy(id = "mat-tab-label-0-1")
    private Button achievementsButton;
    @FindBy(id = "mat-tab-label-0-2")
    private Button requestsButton;
    @FindBy(xpath = "//mat-toolbar/mat-nav-list/button/span[text()='Search']")
    private Button searchButton;
    @FindBy(id = "user-menu")
    private Button menuButton;
    @FindBy(css = "#mat-menu-panel-1 > div > button:nth-child(2)")
    private Button logOutButton;
    @FindBy(css = "#mat-menu-panel-1 > div > button:nth-child(1)")
    private Button myAccountButton;
    @FindBy(xpath = "//*[@id='mat-tab-content-0-0']//button[2]")
    private Button deleteAccountButton;

    @Step("Clicking menu button ...")
    public void clickMenuButton() {
        menuButton.click();
    }

    @Step("Clicking logOut button ...")
    public void clickLogOutButton() {
        logOutButton.click();
    }

    @Step("Verifying if search button is present ...")
    public boolean isSearchButtonDisplayedAndEnabled() {
        return searchButton.isEnabled() && searchButton.isDisplayed();
    }

    @Step("Verifying if account button is present ...")
    public boolean isAccountButtonDisplayedAndEnabled() {
        return menuButton.isEnabled() && menuButton.isDisplayed();
    }

    @Step("Verifying if requests button is present ...")
    public boolean isRequestsButtonDisplayedAndEnabled() {
        return requestsButton.isEnabled() && requestsButton.isDisplayed();
    }

    @Step("Verifying if profile button is present ...")
    public boolean isProfileButtonDisplayedAndEnabled() {
        return profileButton.isEnabled() && profileButton.isDisplayed();
    }

    @Step("Verifying if achievements button is present ...")
    public boolean isAchievementsButtonDisplayedAndEnabled() {
        return achievementsButton.isEnabled() && achievementsButton.isDisplayed();
    }

    @Step("Verifying if user is on account page ...")
    public boolean isUserOnAccountPage() {
        return WebWait.waitForUrlToBePresent(ACCOUNT_PAGE_URL);
    }

    @Step("Clicking delete account button")
    public void clickDeleteAccountButton() {
        deleteAccountButton.click();
    }
}
