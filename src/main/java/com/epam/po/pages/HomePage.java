package com.epam.po.pages;

import com.epam.elements.implementations.Button;
import com.epam.po.BasePage;
import io.qameta.allure.Step;
import org.openqa.selenium.support.FindBy;

import static com.epam.utils.Constants.SPORT_PARTNER_HOME_URL;

public class HomePage extends BasePage {
    @FindBy(xpath = "//button/span[text()='Sign up']/..")
    private Button signUpButton;
    @FindBy(xpath = "//button/span[text()='Login']/..")
    private Button loginButton;
    @FindBy(xpath = "//button/span[text()='Blog']/..")
    private Button blogButton;
    @FindBy(xpath = "//button/span[text()='SportPartner']/..")
    private Button homeButton;

    @Step("Navigating to SportPartner page ...")
    public void navigateToSportPartner() {
        driver.get(SPORT_PARTNER_HOME_URL);
    }

    @Step("Verifying is user on home page ...")
    public boolean isOnHomePage(){
        return driver.getCurrentUrl().contains(SPORT_PARTNER_HOME_URL);
    }

    @Step("Clicking SignUp button ...")
    public void clickSignUpButton(){
        signUpButton.click();
    }

    @Step("Clicking Login button ...")
    public void clickLoginButton(){
        loginButton.click();
    }

    @Step("Clicking Blog button ...")
    public void clickBlogButton(){
        blogButton.click();
    }

    @Step("Clicking Home(SportPartner) button ...")
    public void clickHomeButton(){
        homeButton.click();
    }

    @Step("Verifying if home button is displayed and enabled ...")
    public boolean isHomeButtonDisplayedAndEnabled(){
        return homeButton.isDisplayed() && homeButton.isEnabled();
    }

    @Step("Verifying if Blog button is displayed and enabled ...")
    public boolean isBlogButtonDisplayedAndEnabled(){
        return blogButton.isDisplayed() && blogButton.isEnabled();
    }

    @Step("Verifying if Login button is displayed and enabled ...")
    public boolean isLoginButtonDisplayedAndEnabled(){
        return loginButton.isDisplayed() && loginButton.isEnabled();
    }

    @Step("Verifying if signUp button is displayed and enabled ...")
    public boolean isSignUpButtonDisplayedAndEnabled(){
        return signUpButton.isDisplayed() && signUpButton.isEnabled();
    }

}
