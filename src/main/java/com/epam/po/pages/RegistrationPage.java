package com.epam.po.pages;

import com.epam.elements.implementations.Button;
import com.epam.elements.implementations.TextInput;
import com.epam.po.BasePage;
import io.qameta.allure.Step;
import org.openqa.selenium.support.FindBy;

public class RegistrationPage extends BasePage {
    @FindBy(xpath = "//input[@formcontrolname='email']")
    private TextInput emailInput;
    @FindBy(xpath = "//input[@formcontrolname='password']")
    private TextInput passwordInput;
    @FindBy(xpath = "//input[@formcontrolname='confirmedPassword']")
    private TextInput confirmPasswordInput;
    @FindBy(xpath = "//span[contains(text(),'SUBMIT')]/..")
    private Button submitButton;
    @FindBy(xpath = "//button/span[contains(text(),'FACEBOOK')]/..")
    private Button facebookSignUpButton;
    @FindBy(css = "a[href='/sign-in']")
    private Button signInButton;

    @Step("Filling emailField with email = {0} ...")
    public void fillEmailField(String email){
        emailInput.sendKeys(email);
    }

    @Step("Filling passwordField with password = {0} ...")
    public void fillPasswordField(String password){
        passwordInput.sendKeys(password);
    }

    @Step("Filling ConfirmPasswordField with password = {0} ...")
    public void fillConfirmPasswordField(String password){
        confirmPasswordInput.sendKeys(password);
    }

    @Step("Clicking submit button ...")
    public void clickSubmitButton(){
        submitButton.click();
    }

    @Step("Clicking facebookSignUp button ...")
    public void clickFacebookSignUpButton(){
        facebookSignUpButton.click();
    }

    @Step("Clicking SignIn button ...")
    public void clickSignInButton(){
        signInButton.click();
    }

    @Step("Verifying if login field is displayed and enabled ...")
    public boolean isEmailFieldDisplayedAndEnabled(){
        return emailInput.isDisplayed() && emailInput.isEnabled();
    }

    @Step("Verifying if password field is displayed and enabled ...")
    public boolean isPasswordFieldDisplayedAndEnabled(){
        return passwordInput.isDisplayed() && passwordInput.isEnabled();
    }

    @Step("Verifying if confirmPasswordInput field is displayed and enabled ...")
    public boolean isConfirmPasswordFieldDisplayedAndEnabled(){
        return confirmPasswordInput.isDisplayed() && confirmPasswordInput.isEnabled();
    }

    @Step("Verifying if submit button is displayed and enabled ...")
    public boolean isSubmitButtonDisplayedAndEnabled() {
        return submitButton.isEnabled() && submitButton.isDisplayed();
    }
}
