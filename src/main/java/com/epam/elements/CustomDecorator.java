package com.epam.elements;

import com.epam.elements.implementations.Button;
import com.epam.elements.implementations.TextInput;
import org.openqa.selenium.support.pagefactory.DefaultFieldDecorator;
import org.openqa.selenium.support.pagefactory.ElementLocator;
import org.openqa.selenium.support.pagefactory.ElementLocatorFactory;

import java.lang.reflect.Field;

public class CustomDecorator extends DefaultFieldDecorator {

    public CustomDecorator(ElementLocatorFactory factory) {
        super(factory);
    }

    @Override
    public Object decorate(ClassLoader loader, Field field) {
        ElementLocator locator = factory.createLocator(field);
        if (Button.class.isAssignableFrom(field.getType())) {
            return new Button(proxyForLocator(loader, locator));
        } else if (TextInput.class.isAssignableFrom(field.getType())) {
            return new TextInput(proxyForLocator(loader, locator));
        } else {
            return super.decorate(loader, field);
        }
    }
}
