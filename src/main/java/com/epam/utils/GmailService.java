package com.epam.utils;

import com.google.api.services.gmail.Gmail;
import com.google.api.services.gmail.model.Message;

import java.io.IOException;
import java.util.List;
import java.util.NoSuchElementException;

import static com.epam.utils.Constants.TIME_FOR_LETTER_TO_APPEAR;

public class GmailService {

    private Gmail gmail;

    public GmailService(String email) {
        gmail = GmailConnector.getGmailServiceInstance(email);
    }

    public List<Message> listAllMessages(String email) throws IOException {
        return gmail.users().messages().list(email).execute().getMessages();
    }

    public Message getMessage(String email, String messageId) throws IOException {
        return gmail.users().messages().get(email, messageId).execute();
    }

    public Message getLastMessage(String email) throws IOException, InterruptedException {
        Thread.sleep(TIME_FOR_LETTER_TO_APPEAR);
        String lastMessageId = listAllMessages(email).stream()
                .findFirst()
                .orElseThrow(() -> new NoSuchElementException("Can`t find messages for user with email = " + email))
                .getId();
        return getMessage(email, lastMessageId);
    }
}