package com.epam.utils;

import com.google.api.client.auth.oauth2.Credential;
import com.google.api.client.extensions.java6.auth.oauth2.AuthorizationCodeInstalledApp;
import com.google.api.client.extensions.jetty.auth.oauth2.LocalServerReceiver;
import com.google.api.client.googleapis.auth.oauth2.GoogleAuthorizationCodeFlow;
import com.google.api.client.googleapis.auth.oauth2.GoogleClientSecrets;
import com.google.api.client.googleapis.javanet.GoogleNetHttpTransport;
import com.google.api.client.http.javanet.NetHttpTransport;
import com.google.api.client.json.JsonFactory;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.google.api.client.util.store.FileDataStoreFactory;
import com.google.api.services.gmail.Gmail;
import com.google.api.services.gmail.GmailScopes;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.*;
import java.security.GeneralSecurityException;
import java.util.Collections;
import java.util.List;

import static com.epam.utils.Constants.CREDENTIALS_FILE_NAME;
import static com.epam.utils.Constants.GMAIL_TOKEN_DIRECTORY_PATH;

public class GmailConnector {
    private static final Logger LOGGER = LogManager.getLogger(GmailConnector.class);

    private static final String APPLICATION_NAME = "PAS-UA TAF";
    private static final JsonFactory JSON_FACTORY = JacksonFactory.getDefaultInstance();
    private static final List<String> SCOPES = Collections.singletonList(GmailScopes.GMAIL_READONLY);
    private static final String OFFLINE_ACCESS_TYPE = "offline";
    private static final int DEFAULT_PORT = 8888;

    private static Credential getCredentials(final NetHttpTransport HTTP_TRANSPORT, String userId) throws IOException {
        try (InputStream in = new FileInputStream(CREDENTIALS_FILE_NAME)) {
            GoogleClientSecrets clientSecrets = GoogleClientSecrets.load(JSON_FACTORY, new InputStreamReader(in));
            GoogleAuthorizationCodeFlow flow = new GoogleAuthorizationCodeFlow.Builder(
                    HTTP_TRANSPORT, JSON_FACTORY, clientSecrets, SCOPES)
                    .setDataStoreFactory(new FileDataStoreFactory(new File(GMAIL_TOKEN_DIRECTORY_PATH + userId)))
                    .setAccessType(OFFLINE_ACCESS_TYPE)
                    .build();
            LocalServerReceiver receiver = new LocalServerReceiver.Builder().setPort(DEFAULT_PORT).build();
            return new AuthorizationCodeInstalledApp(flow, receiver).authorize(userId);
        } catch (IOException e) {
            LOGGER.error("Resource not found: " + CREDENTIALS_FILE_NAME);
            return null;
        }
    }

    public static Gmail getGmailServiceInstance(String userId) {
        try {
            final NetHttpTransport HTTP_TRANSPORT = GoogleNetHttpTransport.newTrustedTransport();
            return new Gmail.Builder(HTTP_TRANSPORT, JSON_FACTORY, getCredentials(HTTP_TRANSPORT, userId))
                    .setApplicationName(APPLICATION_NAME)
                    .build();
        } catch (GeneralSecurityException | IOException e) {
            e.printStackTrace();
            return null;
        }
    }
}