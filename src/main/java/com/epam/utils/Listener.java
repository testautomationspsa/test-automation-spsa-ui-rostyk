package com.epam.utils;

import io.qameta.allure.Attachment;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.testng.ITestListener;
import org.testng.ITestResult;

public class Listener implements ITestListener {
    private static final Logger LOGGER = LogManager.getLogger(ITestListener.class);

    @Override
    public void onTestFailure(ITestResult result) {
        saveScreenShotsJPG(DriverManager.getDriver());
        LOGGER.error("Test Failure!!! Screenshot captured for test case: "
                + result.getMethod().getConstructorOrMethod().getName());
    }

    @Attachment(value = "Page screenshot", type = "image/jpg")
    public byte[] saveScreenShotsJPG(WebDriver driver){
        return ((TakesScreenshot) driver).getScreenshotAs(OutputType.BYTES);
    }
}
