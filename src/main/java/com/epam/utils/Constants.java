package com.epam.utils;

public class Constants {
    public static final String SPORT_PARTNER_HOME_URL = "https://sport-partner-88774.firebaseapp.com/home";
    public static final String ACCOUNT_PAGE_URL = "https://sport-partner-88774.firebaseapp.com/account";
    public static final String GMAIL_TOKEN_DIRECTORY_PATH = "src/test/resources/";
    public static final String CREDENTIALS_FILE_NAME = "src/main/resources/clientSecret.json";
    public static final String NEW_USER_EMAIL = "nazar.fedchuk13@gmail.com";
    public static final int TIME_FOR_LETTER_TO_APPEAR = 5000;
    public static final String DEFAULT_PASSWORD = "testTest0";
    public static final int IMPLICITLY_WAIT = 20;
    public static final int EXPLICIT_TIME_OUT = 10;
}
