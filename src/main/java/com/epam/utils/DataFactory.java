package com.epam.utils;

import com.epam.model.User;

import java.io.IOException;

import static com.epam.utils.Constants.DEFAULT_PASSWORD;
import static com.epam.utils.Constants.NEW_USER_EMAIL;

public class DataFactory {

    public static User getUser(){
        return new User("super_admin@gmail.com", DEFAULT_PASSWORD);
    }

    public static User getNewUser(){
        return new User(NEW_USER_EMAIL, DEFAULT_PASSWORD);
    }

    public static String getUsersLastMessage(String email) throws IOException, InterruptedException {
        return new String(new GmailService(email).getLastMessage(email).getPayload().getBody().decodeData())
                .replaceAll("\\r\n","");
    }
}
