package com.epam.utils;

import io.qameta.allure.Allure;
import io.qameta.allure.Attachment;
import io.qameta.allure.model.Status;
import io.qameta.allure.model.StepResult;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.testng.asserts.Assertion;
import org.testng.asserts.IAssert;
import org.testng.collections.Maps;

import java.util.Map;
import java.util.UUID;

public class SoftAssert extends Assertion {
    private static final Logger LOGGER = LogManager.getLogger(SoftAssert.class);
    private final Map<AssertionError, IAssert<?>> errorsMap = Maps.newLinkedHashMap();
    private String assertMessage;

    private static void writeStep(String message, Status status) {
        String uuid = UUID.randomUUID().toString();
        StepResult result = new StepResult().withName(message).withStatus(status);
        Allure.getLifecycle().startStep(uuid, result);
        Allure.getLifecycle().stopStep(uuid);
    }

    @Override
    protected void doAssert(IAssert<?> iAssert) {
        onBeforeAssert(iAssert);
        try {
            assertMessage = iAssert.getMessage();
            iAssert.doAssert();
            onAssertSuccess(iAssert);
        } catch (AssertionError ex) {
            onAssertFailure(iAssert, ex);
            errorsMap.put(ex, iAssert);
            saveScreenshot(assertMessage);
        } finally {
            onAfterAssert(iAssert);
        }
    }

    @Override
    public void onAssertFailure(IAssert iAssert, AssertionError ae) {
        writeStep("[FAILED] " + assertMessage, Status.FAILED);
    }

    public void assertAll() {
        if (!errorsMap.isEmpty()) {
            StringBuilder builder = new StringBuilder("The following asserts failed:");
            for (Map.Entry<AssertionError, IAssert<?>> entry : errorsMap.entrySet()) {
                builder.append("\n\t(" + entry.getKey().getMessage()
                        .replaceAll("did not expect to find \\[true\\] but found \\[false\\]", "") + ")");
            }
            LOGGER.info(builder.toString());
            throw new AssertionError(builder.toString());
        }
    }

    public void saveScreenshot(String assertMessage) {
        LOGGER.error("Test Failure!!! Screenshot captured with message: " + new String(assertMessage.getBytes()));
        saveScreenShotsJPG(DriverManager.getDriver());
    }

    @Attachment(value = "Page screenshot", type = "image/jpg")
    public byte[] saveScreenShotsJPG(WebDriver driver){
        return ((TakesScreenshot) driver).getScreenshotAs(OutputType.BYTES);
    }
}

