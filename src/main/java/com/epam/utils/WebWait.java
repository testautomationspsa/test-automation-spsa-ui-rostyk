package com.epam.utils;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import static com.epam.utils.Constants.EXPLICIT_TIME_OUT;

public class WebWait {

    public static WebElement waitForElementToBeVisible(WebElement element) {
        return (new WebDriverWait(DriverManager.getDriver(), EXPLICIT_TIME_OUT))
                .until(ExpectedConditions.visibilityOf(element));
    }

    public static WebElement waitForElementToBeClickable(WebElement element) {
        return (new WebDriverWait(DriverManager.getDriver(), EXPLICIT_TIME_OUT))
                .until(ExpectedConditions.elementToBeClickable(element));
    }

    public static Boolean waitForUrlToBePresent(String url) {
        return (new WebDriverWait(DriverManager.getDriver(), EXPLICIT_TIME_OUT))
                .until(ExpectedConditions.urlContains(url));
    }

}
