package com.epam.utils;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import io.github.bonigarcia.wdm.WebDriverManager;

import java.util.concurrent.TimeUnit;

import static com.epam.utils.Constants.IMPLICITLY_WAIT;
import static java.util.Objects.isNull;

public class DriverManager {
    private static ThreadLocal<WebDriver> driver = new ThreadLocal<>();

    private DriverManager(){}

    public static WebDriver getDriver(){
        if(isNull(driver.get())){
            initDriver();
        }
        return driver.get();
    }

    private static void initDriver(){
        WebDriverManager.chromedriver().setup();
        driver.set(new ChromeDriver());
        driver.get().manage().timeouts().implicitlyWait(IMPLICITLY_WAIT, TimeUnit.SECONDS);
        driver.get().manage().window().maximize();
    }

    public static void quit(){
        driver.get().quit();
        driver.set(null);
    }

}
