Feature: login and log out on the Sport Partner website

  Scenario Outline: verify the ability to login into Sport Partner
    Given User navigates to Sport Partner website
    Then home, signUp, login and blog buttons are present
    When User presses login button
    Then Login, password fields and submit button are present
    When User fills the login with "<login>" and password with "<password>"
    And User presses login button
    Then Search, profile, achievements, requests and account buttons are present

    Examples:
      | login                      | password  |
      | venue_admin@gmail.com      | testTest0 |
      | coach@gmail.com            | testTest0 |
      | super_admin@gmail.com      | testTest0 |

    Scenario: the ability to still be authorized while refreshing the page
      Given User is authorized
      And User navigates to account page
      When User refreshes the page
      Then account page with all user`s data is present

    Scenario: verify the ability for the authorized user to log out
      Given User is authorized
      When User presses logOut button
      Then signIn form is present

  Scenario Outline: login into Sport Partner with incorrect data
    Given the user is on login page
    When the user enters wrong email "<email>" and password "<password>"
    Then the login submit button is not active

    Examples:
      | email               | password         |
      | fdffd-4@gmailcom    | 1                |
      | fsdgmail.com        | 1111             |
      | super_12mai1223213m | CorrectPAswword1 |

    #login as coach