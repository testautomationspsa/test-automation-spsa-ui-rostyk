Feature:

  Scenario Outline: register into Sport Partner with correct data
    Given the user is on registration page
    When the user register with right email "<email>", password "<password>" and confirmPassword "<confirmPassword>"
    Then the user gets an email with the verification link
    When the user clicks on the link
    Then the account is created and user logins and start using the account
    And account is deleting

    Examples:
      | email                     | password        | confirmPassword |
      | nazar.fedchuk13@gmail.com | testTest0       | testTest0       |
#      | nazar.fedchuk13@gmail.com | TEstingf120fds  | TEstingf120fds  |
#      | nazar.fedchuk13@gmail.com | gskfiwqGJds1230 | gskfiwqGJds1230 |

  Scenario Outline: register into Sport Partner with incorrect data
    Given the user is on registration page
    When the user enters wrong email "<email>", password "<password>" and confirmPassword "<confirmPassword>"
    Then the register submit button is not active

    Examples:
      | email               | password         | confirmPassword   |
      | fdffd-4@gmailcom    | 1                | 14                |
      | fsdgmail.com        | 1111             | 1111              |
      | super_12mai1223213m | CorrectPAswword1 | CorrectPAswword11 |