Feature: search the venues and interact with them

  Scenario Outline: verify ability for authorized user to search venues
    Given User is authorized
    When User presses Search Venue button
    Then The map is present
    When User chooses criteria with Sport type "<sportType>" and Place "<place>"
    And submit the search
    Then the search results based on the criteria are shown on the map

    Examples:
      | sportType | place   |
      | Running   | Outdoor |
      | Running   | Indoor  |
      | Yoga      | Indoor  |
      | Swimming  | Any     |
      | Football  | Outdoor |

  Scenario: browse information about just found venues
    Given there are search results on the map with some venue matching
    When the user clicks on the venue
    Then a popup about the one is present
    When the user clicks on Info button
    Then the popup with all venue detailed information is present

  Scenario: leave a comment to a coach
    Given there is a venue popup with coaches
    When the user click on the coach
    Then a popup with all coach detailed information is present
    When the user leaves a comment
    Then a comment is posted
    When the user refreshes the page
    Then a comment is still posted
