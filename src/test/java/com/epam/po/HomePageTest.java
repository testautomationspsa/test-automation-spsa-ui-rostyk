package com.epam.po;

import com.epam.po.pages.HomePage;
import com.epam.utils.DriverManager;
import com.epam.utils.SoftAssert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.Test;

public class HomePageTest {

    @Test
    public void testIsMainButtonsArePresent(){
        HomePage homePage = new HomePage();
        SoftAssert softAssert = new SoftAssert();
        softAssert.assertTrue(homePage.isHomeButtonDisplayedAndEnabled(),"Home Button is not present");
        softAssert.assertTrue(homePage.isBlogButtonDisplayedAndEnabled(),"Blog Button is not present");
        softAssert.assertTrue(homePage.isLoginButtonDisplayedAndEnabled(),"Login Button is not present");
        softAssert.assertTrue(homePage.isSignUpButtonDisplayedAndEnabled(),"SignUp Button is not present");
        softAssert.assertAll();
    }

    @AfterMethod
    public void closeBrowser(){
        DriverManager.quit();
    }
}
