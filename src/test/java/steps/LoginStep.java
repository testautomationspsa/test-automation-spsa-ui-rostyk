package steps;

import com.epam.bo.AccountBO;
import com.epam.bo.HomeBO;
import com.epam.bo.LoginBO;
import com.epam.model.User;
import com.epam.utils.DriverManager;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

import static com.epam.utils.DataFactory.getUser;

public class LoginStep {
    private HomeBO homeBO = new HomeBO();
    private LoginBO loginBO = new LoginBO();
    private AccountBO accountBO = new AccountBO();

    @Given("^User navigates to Sport Partner website$")
    public void user_navigates_to_Sport_Partner_website() {
        homeBO.navigateToHomePage();
    }

    @Then("^home, signUp, login and blog buttons are present$")
    public void home_signUp_login_and_blog_buttons_are_present() {
        homeBO.verifyIfAllNecessaryButtonsArePresent();
    }

    @When("^User presses login button$")
    public void user_presses_login_button() {
        homeBO.goToLoginPage();
    }

    @Then("^Login, password fields and submit button are present$")
    public void login_password_fields_and_submit_button_are_present() {
        loginBO.verifyIfAllNecessaryFieldArePresent();
    }

    @When("^User fills the login with \"([^\"]*)\" and password with \"([^\"]*)\"$")
    public void user_fills_the_login_with_and_password_with(String login, String password) {
        loginBO.authorize(login, password);
    }

    @Then("^Search, profile, achievements, requests and account buttons are present$")
    public void search_profile_achievements_requests_and_account_buttons_are_present() {
        accountBO.verifyIfAllNecessaryButtonsArePresent();
        DriverManager.quit();
    }

    @Given("^User is authorized$")
    public void user_is_authorized() {
        homeBO.navigateToHomePage();
        homeBO.goToLoginPage();
        User user = getUser();
        loginBO.authorize(user.getEmail(), user.getPassword());
    }

    @Given("^User navigates to account page$")
    public void user_navigates_to_account_page() {
        accountBO.verifyIfUserIsOnAccountPage();
    }

    @When("^User refreshes the page$")
    public void user_refreshes_the_page() {
        DriverManager.getDriver().navigate().refresh();
    }

    @Then("^account page with all user`s data is present$")
    public void account_page_with_all_user_s_data_is_present() {
        accountBO.verifyIfAllNecessaryButtonsArePresent();
        DriverManager.quit();
    }

    @When("^User presses logOut button$")
    public void user_presses_logOut_button() {
        accountBO.logOut();
    }

    @Then("^signIn form is present$")
    public void signin_form_is_present() {
        loginBO.verifyIfAllNecessaryFieldArePresent();
        DriverManager.quit();
    }

    @Given("the user is on login page")
    public void the_user_is_on_login_Page() {
        homeBO.navigateToHomePage();
        homeBO.goToLoginPage();
    }

    @When("the user enters wrong email \"([^\"]*)\" and password \"([^\"]*)\"$")
    public void theUserEntersWrongEmailAndPassword(String email, String password) {
        loginBO.fillEmailAndPassword(email, password);
    }

    @Then("the login submit button is not active")
    public void theLoginSubmitButtonIsNotActive() {
        loginBO.verifySubmitButton(false);
        DriverManager.quit();
    }
}
