package steps;

import com.epam.bo.AccountBO;
import com.epam.bo.HomeBO;
import com.epam.bo.LoginBO;
import com.epam.bo.RegisterBO;
import com.epam.utils.DriverManager;
import com.epam.utils.GmailService;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import cucumber.api.java.en_old.Ac;

import java.io.IOException;

import static com.epam.utils.Constants.DEFAULT_PASSWORD;
import static com.epam.utils.Constants.NEW_USER_EMAIL;
import static io.restassured.RestAssured.given;

public class RegisterStep {
    private HomeBO homeBO = new HomeBO();
    private RegisterBO registerBO = new RegisterBO();
    private LoginBO loginBO = new LoginBO();
    private AccountBO accountBO = new AccountBO();

    @Given("the user is on registration page")
    public void theUserIsOnRegistrationPage() {
        homeBO.navigateToHomePage();
        homeBO.goToRegisterPage();
    }

    @When("the user register with right email \"([^\"]*)\", password \"([^\"]*)\" and confirmPassword \"([^\"]*)\"$")
    public void theUserRegisterWithRightEmailPasswordAndConfirmPassword(String email, String password, String confirmPassword) {
        registerBO.register(email, password, confirmPassword);
    }

    @Then("the user gets an email with the verification link")
    public void theUserGetsAnEmailWithTheVerificationLink() throws IOException, InterruptedException {
        registerBO.verifyIfActivationLinkIsSentToEmail();
    }

    @When("the user clicks on the link")
    public void theUserClicksOnTheLink() {
        registerBO.activateUser();
    }

    @Then("the account is created and user logins and start using the account")
    public void theAccountIsCreatedAndUserLoginsAndStartUsingTheAccount() {
        homeBO.goToLoginPage();
        loginBO.authorize(NEW_USER_EMAIL, DEFAULT_PASSWORD);
        accountBO.verifyIfUserIsOnAccountPage();
        accountBO.verifyIfAllNecessaryButtonsArePresent();
    }

    @And("account is deleting")
    public void accountIsDeleting() {
        accountBO.deleteAccount();
        loginBO.verifyIfAllNecessaryFieldArePresent();
        DriverManager.quit();
    }

    @When("the user enters wrong email \"([^\"]*)\", password \"([^\"]*)\" and confirmPassword \"([^\"]*)\"")
    public void theUserEntersWrongEmailPasswordAndConfirmPassword(String email, String password, String confirmPassword) {
        registerBO.fillEmailAndPassword(email, password, confirmPassword);
    }


    @Then("the register submit button is not active")
    public void theRegisterSubmitButtonIsNotActive() {
        registerBO.verifySubmitButton(false);
        DriverManager.quit();
    }
}
