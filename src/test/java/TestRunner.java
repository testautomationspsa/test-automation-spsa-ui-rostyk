import com.epam.utils.Listener;
import cucumber.api.CucumberOptions;
import cucumber.api.testng.AbstractTestNGCucumberTests;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Listeners;

@CucumberOptions(features = {"src/test/resources/features/login.feature"},
        glue = {"steps"})
@Listeners({Listener.class})
public class TestRunner extends AbstractTestNGCucumberTests {
    @DataProvider(parallel = true)
    public Object[][] scenarios() {
        return super.scenarios();
    }
}